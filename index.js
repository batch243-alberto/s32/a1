let http = require("http")

const port = 4000 

http.createServer(function(request,response){

	if (request.url == '/' && request.method == "GET"){
	response.writeHead(200,{'Content-type': 'text/plain'});
	response.end("Welcome to Booking System")
	}

}).listen(4000)

console.log("Server running at localhost: 4000");